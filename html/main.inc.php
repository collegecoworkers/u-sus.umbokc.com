<?php

    /*!
     * ifsoft.co.uk v1.1
     *
     * http://ifsoft.com.ua, http://ifsoft.co.uk
     * qascript@ifsoft.co.uk
     *
     * Copyright 2012-2017 Demyanchuk Dmitry (https://vk.com/dmitry.demyanchuk)
     */

    if ($auth->authorize(auth::getCurrentUserId(), auth::getAccessToken())) {

        header("Location: /account/wall");
    }

    $page_id = "main";

    $css_files = array("main.css", "homepage.css?x=1", "my.css");
    $page_title = APP_TITLE;

    include_once("../html/common/header.inc.php");

?>

<body class="home first-page">

    <?php

        include_once("../html/common/topbar.inc.php");
    ?>

    <div class="content-page homepage">

        <div class="wrap" style="padding: 10vh 10vw;">
            <div class="homepage-section-2">
                <div class="homepage-section-content">
                    <h1 class="homepage-section-headline"><?php echo $LANG['label-missing-account']; ?></h1>
                    <p class="homepage-section-description">Социальная сеть для работодателей поможет наладить деловые контакты, а также с помощью данной системы работодатели смогут отслеживать свои будущие кадры, что в дальнейшем позволить найти лучших сотрудников в свою организацию.</p>
                    <div class="homepage-cta homepage-spacing">
                        <a href="/signup" class="button"><?php echo $LANG['action-join']; ?></a>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <?php

        include_once("../html/common/footer.inc.php");
    ?>


</body
</html>
