<section class="standard-page">
    <h1>Соглашение и правила пользования сайтом <?php echo APP_TITLE; ?></h1>
    <p>Настоящее Соглашение регламентирует отношения между Администрацией информационного ресурса «<?php echo APP_TITLE; ?>» и физическим лицом, которое ищет и распространяет информацию на данном ресурсе.</p>
    <p>Информационный ресурс «<?php echo APP_TITLE; ?>» не является средством массовой информации, Администрация ресурса не осуществляет редактирование размещаемой информации и не несет ответственность за ее содержание.</p>
    <p>Пользователь, разместивший информацию на ресурсе «<?php echo APP_TITLE; ?>», самостоятельно представляет и защищает свои интересы, возникающие в связи с размещением указанной информации, в отношениях с третьими лицами.</p>
</section>
